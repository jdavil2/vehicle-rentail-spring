/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package home.personal.repository;

import home.personal.entity.Employee;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author josse
 */
public interface EmployeeRepository extends JpaRepository<Employee, UUID>{
    List<Employee> findByManager(String manager);
    Employee findByUsername(String username);
}
