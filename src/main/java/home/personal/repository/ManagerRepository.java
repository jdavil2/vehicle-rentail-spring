/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package home.personal.repository;

import home.personal.entity.Manager;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author josse
 */
@Repository
public interface ManagerRepository extends JpaRepository <Manager, UUID> {
    Manager findByUsername(String username);
}
