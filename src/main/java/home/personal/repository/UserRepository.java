/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.repository;

import home.personal.entity.User;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Larvitar
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID>{
    User findByUsername(String username);
}
