/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package home.personal.repository;

import home.personal.entity.Vehicles;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author josse
 */
public interface VehicleRepository extends JpaRepository<Vehicles, String>{
    Vehicles findByVin(String vin);
}
