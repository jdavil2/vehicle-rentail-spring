/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.repository;

import home.personal.entity.Customer;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Larvitar
 */
@Transactional
@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID>{
    Customer findByUsername(String username);
}
