/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package home.personal.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author josse
 */
@Entity
@Table(name = "vehicle")
public class Vehicles implements Serializable {
    @Id
    protected String vin;
    protected String vehicletype;
    
    public String getVin(){
        return vin;
    }
    public void setVin(String vin){
        this.vin = vin;
    }
    // temporary, will be removed once subclasses are made
    public void setVehicletype(String vehicletype){
        this.vehicletype = vehicletype;
    }
    
    public String getVehicletype(){
        return vehicletype;
    }
    @Override
    public String toString(){
        return "VIN: " + vin + "\nType: " + vehicletype;
    }
}
