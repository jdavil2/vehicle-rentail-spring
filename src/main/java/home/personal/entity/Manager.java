/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package home.personal.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author josse
 */
@Entity
@Table(name="Manager")
public class Manager extends Employee{
    @Override
    public String toString(){
        return "username: " + username + "\n" +user_type + "\nid:" + id + "\ndate hired: " + date_hired + "\nmanages: " + location;
    }
}
