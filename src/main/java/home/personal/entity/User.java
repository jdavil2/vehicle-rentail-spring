/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.entity;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author Larvitar
 */
@Entity
@Table(name = "users")
@Inheritance (strategy = InheritanceType.JOINED)
public class User implements Serializable {
    
    protected UUID id;
    protected String username;
    protected String password;
    protected String user_type;
    
    
    @Id @GeneratedValue
    public UUID getId(){
        return id;
    }
     public void setId(UUID id){
         this.id = id;
     }
     public String getUsername(){
         return username;
     }
     public void setUsername(String username){
         this.username = username;
     }
     public String getPassword(){
         return password;
     }
     public void setPassword(String password){
         this.password = password;
     }
    
     public String getUser_type(){
         return user_type;
     }
     
     public void setUser_type(String user_type){
         this.user_type = user_type;
     }
     
    @Override
     public String toString(){
         return "username: " + username + "\nid:" + id;
     }
    
}
