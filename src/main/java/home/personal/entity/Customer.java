/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.entity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Larvitar
 */
@Entity
@Table(name = "customer")
public class Customer extends User{

    private boolean renting;
    private String location;
    private String accountname;
    @OneToOne
    @JoinColumn
    private Vehicles vehicle;

    public void setRenting(boolean rent) {
        this.renting = rent;
    }

    public boolean getRenting() {
        /**
         * when vehicle is implemented this will be altered to return the
         * vehicles that the customer is renting
         */
        return renting;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public Vehicles getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicles vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder(super.toString() + "\nCustomer location: " + this.location + "\nVehicle rented: " + this.renting);

        returnString.append("\nVehicle Rented: ").append(vehicle);

        if (accountname != null) {
            returnString.append("\nCompany worked for: ").append(accountname);
        }
        return returnString.toString();
    }
}
