/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package home.personal.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author josse
 */
@Entity
@Table(name = "account")
public class Account implements Serializable{
    
    @Id
    protected String accountname;
    protected int numberofemployees;
    @OneToMany
    @JoinColumn(name = "accountname")
    protected List<Customer> employees;
    
    public String getAccountname(){
        return accountname;
    }
    
    public void setAccountname(String accountname){
        this.accountname = accountname;
    }
    
    public int getNumberofemployees(){
        return numberofemployees;
    }
    
    public void setNumberofemployees(int numberofemployees){
        this.numberofemployees = numberofemployees;
    }
    /*
    public List<Customer> getEmployees(){
        return employees;
    }*/
    @Override
    public String toString(){
        StringBuilder emp = new StringBuilder("Company name: " + accountname + "\nNumber of employees: " + employees.size());
        System.out.println(employees.size());
        employees.forEach(i ->{emp.append("\n\tEmployee: ").append(i.getUsername());});
        return emp.toString();
    }
}
