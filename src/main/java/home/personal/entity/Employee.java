/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package home.personal.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author josse
 */
@Entity
@Table(name = "employee")
public class Employee extends User{
    protected String location;
    protected LocalDate date_hired;
    protected String manager;
    
    public void setLocation(String location){
        this.location = location;
    }
    
    public String getLocation(){
        return location;
    }
    
    public LocalDate getDate_hired(){
        return date_hired;
    }
    
    public void setDate_hired(LocalDate date_hired){
        this.date_hired = date_hired;
    }
    public String getManager(){
        return manager;
    }
    public void setManager(String manager){
        this.manager = manager;
    }
    @Override
    public String toString(){
        return super.toString() + "\nWorks at: " + location + "\nHired at: " + date_hired + "\nManager: " + manager;
    }
}
