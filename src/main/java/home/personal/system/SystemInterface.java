/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.system;

import home.personal.entity.*;
import home.personal.repository.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Larvitar
 */
@Transactional
@Service
public class SystemInterface {
    private final CustomerRepository customerRepo;
    private final UserRepository userRepo;
    private final EmployeeRepository employeeRepo;
    private final ManagerRepository managerRepo;
    private final AccountRepository accountRepo;
    private final VehicleRepository vehicleRepo;
    
    @Autowired
    public SystemInterface(CustomerRepository customerRepo, UserRepository userRepo, EmployeeRepository employeeRepo, ManagerRepository managerRepo, AccountRepository accountRepo, VehicleRepository vehicleRepo){
        this.customerRepo = customerRepo;
        this.userRepo = userRepo;
        this.employeeRepo = employeeRepo;
        this.managerRepo = managerRepo;
        this.accountRepo = accountRepo;
        this.vehicleRepo = vehicleRepo;
    }
    
    public List<Customer> getAllCustomers(){
        return (List<Customer>) customerRepo.findAll();
    }
    
    public Customer findCustomer(String username){
        return customerRepo.findByUsername(username);
    }
    
    public List<User> getAllUsers(){
        return userRepo.findAll();
    }
    
    public void saveUser(User user){
        userRepo.save(user);
    }
    
    public void saveCustomer(Customer customer){
        customerRepo.save(customer);
    }
    
    public User findUser(String username){
        return userRepo.findByUsername(username);
    }
    public User getUser(User user){
        User currUser = userRepo.findByUsername(user.getUsername());
        switch(currUser.getUser_type()){
            case "Customer":
                return customerRepo.findById(currUser.getId()).get();
            case "Manager":
                System.out.println("Manager");
                return managerRepo.getById(currUser.getId());
            case "Employee":
                return employeeRepo.getById(currUser.getId());
            default:
                System.out.println(currUser.getUser_type());
        }
        return currUser;
    }
    public Employee findEmployee(String employee){
        return employeeRepo.findByUsername(employee);
    }
    public List<Employee> findEmployeesManagedBy(String manager){
        return employeeRepo.findByManager(manager);
    }
    public List<Employee> getAllEmployees(){
        return employeeRepo.findAll();
    }
    
    public void saveEmployee(Employee employee){
        employeeRepo.save(employee);
    }
    
    public void saveManager(Manager manager){
        managerRepo.save(manager);
    }
    public List<Manager> getAllManagers(){
        return managerRepo.findAll();
    }
    public Manager findManager(String username){
        return managerRepo.findByUsername(username);
    }
    public void getAccount(){
        Account a = accountRepo.findByAccountname("Fire Nation");
        System.out.println(a);
    }
    
    public void saveVehicle(Vehicles v){
        vehicleRepo.save(v);
    }
    public void printAllVehicles(){
        List<Vehicles> v = vehicleRepo.findAll();
        v.forEach(h->{
            System.out.println(h);
        });
    }
    
    /**
     * for some reason, I am unable to figure out what is wrong with this
     * update logic, some stalestateexception is causing it, going through debugger
     * at the moment doesn't appear to help
     * @param cust 
     */
    @Transactional
    public void rentVehicle(Customer cust){
        cust.setUsername("King Korbin"); 
    }
    public Vehicles findVehicle(String vin){
        return vehicleRepo.findByVin(vin);
    }
    
    public void addVehicle(String vin, String vehicletype, Customer cust){
        Vehicles vehicle = new Vehicles();
        vehicle.setVin(vin);
        vehicle.setVehicletype(vehicletype.toUpperCase());
        System.out.println(vehicle);
        //vehicleRepo.save(vehicle);
    }
}