/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.personal.web;

import home.personal.entity.*;
import home.personal.system.SystemInterface;
import java.util.List;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Larvitar
 */
@Controller
public class DemoController {
    
    private final SystemInterface system;
    
    public DemoController(SystemInterface system){
        this.system = system;
    }
    
    @GetMapping("/")
    public String root(ModelMap model){
        List<User> allUsers = system.getAllUsers();
        User user = new User();
        model.put("allUsers", allUsers);
        model.put("user", user);
        return "login";
    }
    @PostMapping("/")
    public String rootPost(@RequestParam("action")String action, User user, ModelMap model){
        if(action.equals("Register"))
        {
            system.saveUser(user);
            return "/registered";
        }
        if(system.findUser(user.getUsername()) == null)
        {
            return "user_not_found";
        }
        User k = system.getUser(user);
        
        /**
         * basic password check, if the user is found within database check the 
         * input password against the stored password and if they're different return 
         * user not found, should probably change it to an incorrect username/password
         * page
         */
        if(!k.getPassword().equals(user.getPassword()))
        {
            return "user_not_found";
        }
        // confirmed that the user is the correct user and data populates correctly
        model.put("user", k);
        /*
        Customer o = system.findCustomer("King Corbin");
        system.rentVehicle(o);
        System.out.println(o);*/
        return "/hello";
    }
    
    @GetMapping("/login")
    public String login(ModelMap model){
        List <User> allUsers = system.getAllUsers();
        User user = allUsers.get(1);
        model.put("user", user);
        return "user_not_found";
    }
    
    @PostMapping("/registered")
    public String registered()
    {
        return"registered";
    }
    
    @PostMapping("/error")
    public String error()
    {
        return "error";
    }
}
